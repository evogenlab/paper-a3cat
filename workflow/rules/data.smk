from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider


HTTP = HTTPRemoteProvider()

rule get_catalog_of_life:
    '''
    '''
    input:
        HTTP.remote(config['catalog_of_life_url'], keep_local=False)
    output:
        'resources/col_taxa.tsv'
    log:
        'logs/get_catalog_of_life.txt'
    shadow: 'minimal'
    shell:
        'unzip {input} 2>&1 > {log};'
        'mv Taxon.tsv {output} 2>> {log}'


rule compute_time_data:
    '''
    '''
    input:
        config['summary_file_path']
    output:
        'results/data/time_data.tsv'
    log:
        'logs/compute_time_data.txt'
    conda:
        '../envs/data.yaml'
    script:
        '../scripts/compute_time_data.py'


rule download_ete3_db:
    '''
    '''
    output:
        'results/data/.ete3db'
    log:
        'logs/download_ete3_db.txt'
    conda:
        '../envs/data.yaml'
    script:
        '../scripts/download_ete3_db.py'


rule generate_figure1_data:
    '''
    '''
    input:
        col_taxa_path = rules.get_catalog_of_life.output,
        name_matching_file_path = config['name_matching_file_path'],
        ete3_db_path = rules.download_ete3_db.output
    output:
        tree = 'results/data/figure_1_tree.nwk',
        species_numbers = 'results/data/figure_1_species.tsv'
    log:
        'logs/generate_figure1_data.txt'
    conda:
        '../envs/data.yaml'
    params:
        base_taxon = 'Arthropoda'
    script:
        '../scripts/generate_figure1_data.py'


rule generate_figure2_tree:
    '''
    '''
    input:
        summary = config['summary_file_path'],
        ete3_db_path = rules.download_ete3_db.output
    output:
        'results/data/figure_2_tree.nwk'
    log:
        'logs/generate_figure2_tree.txt'
    conda:
        '../envs/data.yaml'
    script:
        '../scripts/generate_figure2_tree.py'


rule get_paper_numbers:
    '''
    '''
    input:
        summary = config['summary_file_path'],
        species_data = rules.generate_figure1_data.output.species_numbers,
        ete3_db_path = rules.download_ete3_db.output,
        col_taxa_path = rules.get_catalog_of_life.output
    output:
        'results/data/paper_numbers.tsv'
    log:
        'logs/get_paper_numbers.txt'
    conda:
        '../envs/data.yaml'
    params:
        min_assemblies_per_order = config['numbers']['min_assemblies_per_order'],
        min_assemblies_per_species = config['numbers']['min_assemblies_per_species'],
        base_taxon = 'Arthropoda'
    script:
        '../scripts/get_paper_numbers.py'


rule filter_orders:
    '''
    '''
    input:
        summary = config['summary_file_path'],
        tree = rules.generate_figure2_tree.output
    output:
        'results/data/orders_data.rda'
    log:
        'logs/filter_orders.txt'
    conda:
        '../envs/figures.yaml'
    params:
        orders_palette = config['figures']['orders_color_palette'],
        min_assemblies_per_order = config['figures']['min_assemblies_per_order']
    script:
        '../scripts/R/filter_orders.R'
