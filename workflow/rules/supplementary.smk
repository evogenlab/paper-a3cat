rule supp_figure2_assembly_levels:
    '''
    '''
    input:
        orders_data = rules.filter_orders.output,
        time_data = rules.compute_time_data.output
    output:
        'results/supp_figure2_assembly_levels.png'
    log:
        'logs/supp_figure2_assembly_levels.txt'
    conda:
        '../envs/figures.yaml'
    script:
        '../scripts/R/supp_figure2_assembly_levels.R'


rule supp_figure3_duplicated_buscos:
    '''
    '''
    input:
        orders_data = rules.filter_orders.output,
        time_data = rules.compute_time_data.output
    output:
        'results/supp_figure3_duplicated_buscos.png'
    log:
        'logs/supp_figure3_duplicated_buscos.txt'
    conda:
        '../envs/figures.yaml'
    params:
        lineage = config['supplementary']['supp_figure3_duplicated_buscos']['lineage'],
        metric = config['supplementary']['supp_figure3_duplicated_buscos']['metric'],
        min_assembly_size = config['figures']['min_assembly_size']
    script:
        '../scripts/R/busco_vs_assembly_stats.R'


rule supp_figure4_fragmented_buscos:
    '''
    '''
    input:
        orders_data = rules.filter_orders.output,
        time_data = rules.compute_time_data.output
    output:
        'results/supp_figure4_fragmented_buscos.png'
    log:
        'logs/supp_figure4_fragmented_buscos.txt'
    conda:
        '../envs/figures.yaml'
    params:
        lineage = config['supplementary']['supp_figure4_fragmented_buscos']['lineage'],
        metric = config['supplementary']['supp_figure4_fragmented_buscos']['metric'],
        min_assembly_size = config['figures']['min_assembly_size']
    script:
        '../scripts/R/busco_vs_assembly_stats.R'


rule supp_figure5_lineage_comparisons:
    '''
    '''
    input:
        orders_data = rules.filter_orders.output,
        v3_summary = config['v3_summary_file_path'],
    output:
        'results/supp_figure5_lineage_comparisons.png'
    log:
        'logs/supp_figure5_lineage_comparisons.txt'
    conda:
        '../envs/figures.yaml'
    params:
        lineage_1 = config['supplementary']['supp_figure5_lineage_comparisons']['lineage_1'],
        lineage_2 = config['supplementary']['supp_figure5_lineage_comparisons']['lineage_2'],
        lineage_3 = config['supplementary']['supp_figure5_lineage_comparisons']['lineage_3'],
        metric = config['supplementary']['supp_figure5_lineage_comparisons']['metric']
    script:
        '../scripts/R/lineage_comparisons.R'
