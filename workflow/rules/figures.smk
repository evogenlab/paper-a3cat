
rule figure1_full_orders_tree:
    '''
    '''
    input:
        orders_data = rules.filter_orders.output,
        orders_tree = rules.generate_figure1_data.output.tree,
        species_numbers = rules.generate_figure1_data.output.species_numbers,
        time_data = rules.compute_time_data.output,
    output:
        'results/figure1_full_orders_tree.png'
    log:
        'logs/figure1_full_orders_tree.txt'
    conda:
        '../envs/figures.yaml'
    script:
        '../scripts/R/figure1_full_orders_tree.R'


rule figure2_stats_per_order:
    '''
    '''
    input:
        orders_data = rules.filter_orders.output
    output:
        'results/figure2_stats_per_order.png'
    log:
        'logs/figure2_stats_per_order.txt'
    conda:
        '../envs/figures.yaml'
    params:
        lineage = config['figures']['figure2_stats_per_order']['lineage'],
        metric = config['figures']['figure2_stats_per_order']['metric']
    script:
        '../scripts/R/figure2_stats_per_order.R'


rule figure3_busco_vs_assembly_stats:
    '''
    '''
    input:
        orders_data = rules.filter_orders.output
    output:
        'results/figure3_busco_vs_assembly_stats.png'
    log:
        'logs/figure3_busco_vs_assembly_stats.txt'
    conda:
        '../envs/figures.yaml'
    params:
        lineage = config['figures']['figure3_busco_vs_assembly_stats']['lineage'],
        metric = config['figures']['figure3_busco_vs_assembly_stats']['metric'],
        min_assembly_size = config['figures']['min_assembly_size'],
        busco_threshold = config['figures']['busco_threshold']
    script:
        '../scripts/R/busco_vs_assembly_stats.R'


rule figure4_lineage_comparisons:
    '''
    '''
    input:
        orders_data = rules.filter_orders.output,
        v3_summary = config['v3_summary_file_path'],
    output:
        'results/figure4_lineage_comparisons.png'
    log:
        'logs/figure4_lineage_comparisons.txt'
    conda:
        '../envs/figures.yaml'
    params:
        lineage_1 = config['figures']['figure4_lineage_comparisons']['lineage_1'],
        lineage_2 = config['figures']['figure4_lineage_comparisons']['lineage_2'],
        lineage_3 = config['figures']['figure4_lineage_comparisons']['lineage_3'],
        metric = config['figures']['figure4_lineage_comparisons']['metric']
    script:
        '../scripts/R/lineage_comparisons.R'
