import logging
from ete3 import NCBITaxa
from utils import setup_logging

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    summary_file_path = snakemake.input.summary
    ete3_db_path = snakemake.input.ete3_db_path[0]
    tree_file_path = snakemake.output[0]

    logging.info('Getting list of orders from summary file')
    with open(summary_file_path) as summary_file:
        tmp = summary_file.readline()[:-1].split('\t')
        order_index = tmp.index('Order')
        orders = set(line.split('\t')[order_index] for line in summary_file)

    # In case "other" is present as an order, remove it
    if 'Other' in orders:
        orders.remove('Other')

    logging.info(f'Found {len(orders)} orders:')
    for order in orders:
        logging.info(f'    - {order}')

    logging.info(f'Generating tree')

    # Generate phylogenetic tree from list of orders using ete3
    tree_file = open(tree_file_path, 'w')

    logging.info('Initializing ete3 database')
    ncbi = NCBITaxa(dbfile=ete3_db_path)

    orders_taxa_id = ncbi.get_name_translator(orders)
    orders_taxa_id = [i for s in orders_taxa_id.values() for i in s]
    tree = ncbi.get_topology(orders_taxa_id, rank_limit='order')
    for node in tree.traverse():
        node.name = node.sci_name

    tree_file.write(tree.write(format=4))

    logging.info(f'Succesfully saved tree to <{tree_file_path}>')
