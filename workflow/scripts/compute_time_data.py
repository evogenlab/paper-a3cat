import datetime
import logging
from collections import defaultdict, Counter
from utils import setup_logging

DATE_HEADER = 'Submission Date'
ACCESSION_HEADER = 'Genbank Accession'
TAXID_HEADER = 'TaxId'
ORDER_HEADER = 'Order'
ASSEMBLY_LEVEL_HEADER = 'Assembly Level'

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    summary_file_path = snakemake.input[0]
    output_file_path = snakemake.output[0]

    monthly_data = defaultdict(lambda: {'assemblies': set(), 'species': set(), 'order': set(), 'levels': []})
    with open(summary_file_path) as summary_file:
        header = summary_file.readline().split('\t')
        date_index = header.index(DATE_HEADER)
        accession_index = header.index(ACCESSION_HEADER)
        taxid_index = header.index(TAXID_HEADER)
        order_index = header.index(ORDER_HEADER)
        assembly_level_index = header.index(ASSEMBLY_LEVEL_HEADER)
        for line in summary_file:
            fields = line.split('\t')
            date, accession, taxid, order, level = fields[date_index], fields[accession_index], fields[taxid_index], fields[order_index], fields[assembly_level_index]
            date = datetime.datetime.strptime(date, '%Y-%m-%d').strftime('%Y-%m')
            monthly_data[date]['assemblies'].add(accession)
            monthly_data[date]['species'].add(taxid)
            monthly_data[date]['order'].add(order)
            monthly_data[date]['levels'].append(level)

    with open(output_file_path, 'w') as output_file:
        output_file.write('Date\tMonths\tAssemblies\tSpecies\tOrder\tChromosome\tScaffold\tContig\n')
        start_date = datetime.datetime.strptime(sorted(monthly_data.keys())[0], '%Y-%m')
        current_assemblies_pool = set()
        current_species_pool = set()
        current_orders_pool = set()
        current_levels = []
        for i, month_data in enumerate(sorted(monthly_data.items())):
            month, data = month_data
            date = datetime.datetime.strptime(month, '%Y-%m')
            month_delta = (date.year - start_date.year) * 12 + date.month - start_date.month
            current_assemblies_pool = current_assemblies_pool.union(data['assemblies'])
            current_species_pool = current_species_pool.union(data['species'])
            current_orders_pool = current_orders_pool.union(data['order'])
            current_levels += data['levels']
            current_levels_counts = Counter(current_levels)
            output_file.write(f'{month}\t{month_delta}\t{len(current_assemblies_pool)}\t{len(current_species_pool)}\t{len(current_orders_pool)}\t')
            n_chromosome = current_levels_counts['Chromosome'] if 'Chromosome' in current_levels_counts else 0
            n_scaffold = current_levels_counts['Scaffold'] if 'Scaffold' in current_levels_counts else 0
            n_contig = current_levels_counts['Contig'] if 'Contig' in current_levels_counts else 0
            output_file.write(f'{n_chromosome}\t{n_scaffold}\t{n_contig}\n')
