import pandas
import logging
from ete3 import NCBITaxa
from utils import setup_logging, is_base_taxon, find_order

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    summary_file_path = snakemake.input.summary
    species_data_file_path = snakemake.input.species_data
    ete3_db_path = snakemake.input.ete3_db_path[0]
    col_taxa_path = snakemake.input.col_taxa_path[0]
    output_file_path = snakemake.output[0]
    base_taxon = snakemake.params.base_taxon
    min_assemblies_per_order = snakemake.params.min_assemblies_per_order
    min_assemblies_per_species = snakemake.params.min_assemblies_per_species

    # Output file
    output_file = open(output_file_path, 'w')

    # Load species numbers data with pandas
    species_numbers_data = pandas.read_csv(species_data_file_path, sep='\t')

    # Load summary data with pandas
    data = pandas.read_csv(summary_file_path, sep='\t')

    # Number of assemblies
    logging.info(f'Computing general numbers')
    output_file.write('## General numbers\n')
    n_assemblies_total = data.shape[0]
    output_file.write(f'Total number of assemblies: {n_assemblies_total}\n')

    # Species data
    logging.info(f'Computing species numbers')
    species_data = data.drop_duplicates(subset='Species')
    n_species_total = species_data.shape[0]
    output_file.write(f'Total number of species represented: {n_species_total}\n')

    # Orders data
    logging.info(f'Computing general order numbers')
    n_orders_total = len(data['Order'].unique())
    output_file.write(f'Total number of orders represented: {n_orders_total}\n')
    order_species_counts = species_data['Order'].value_counts(dropna=False)
    n_orders_min_species = len(order_species_counts[order_species_counts >= min_assemblies_per_order])
    output_file.write(f'Number of orders with at least {min_assemblies_per_order} species: {n_orders_min_species}\n')
    order_assemblies_counts = data['Order'].value_counts(dropna=False)
    n_orders_min_assemblies = len(order_assemblies_counts[order_assemblies_counts >= min_assemblies_per_order])
    output_file.write(f'Number of orders with at least {min_assemblies_per_order} assemblies: {n_orders_min_assemblies}\n')
    n_assemblies_in_retained_orders = sum(order_assemblies_counts[order_assemblies_counts >= min_assemblies_per_order])
    output_file.write(f'Number of assemblies in orders with at least {min_assemblies_per_order} assemblies: {n_assemblies_in_retained_orders}\n')
    n_species_in_retained_orders = sum(order_species_counts[order_species_counts >= min_assemblies_per_order])
    output_file.write(f'Number of species in orders with at least {min_assemblies_per_order} assemblies: {n_species_in_retained_orders}\n')

    # CoL and NCBI numbers
    logging.info(f'Getting descendants for "{base_taxon}" from NCBI')
    ncbi = NCBITaxa(dbfile=ete3_db_path)
    descendants = ncbi.get_descendant_taxa(base_taxon, intermediate_nodes=True, rank_limit='species', collapse_subspecies=True)
    ranks = ncbi.get_rank(descendants)
    n_species_total_ncbi = len(tuple(descendant for descendant in descendants if ranks[descendant] == 'species'))
    output_file.write(f'Total number of Arthropod species in NCBI: {n_species_total_ncbi}\n')
    n_orders_total_ncbi = len(tuple(descendant for descendant in descendants if ranks[descendant] == 'order'))
    output_file.write(f'Total number of Arthropod orders in NCBI: {n_orders_total_ncbi}\n')

    logging.info(f'Parsing CoL to retrieve all ranks above species')
    col_taxa = {}
    with open(col_taxa_path) as taxon_file:
        taxon_file.readline()
        for line in taxon_file:
            fields = line.split('\t')
            if fields[6] != 'species':
                taxon_id = fields[0]
                parent_id = fields[1]
                taxon_name = fields[7].split(' ')[0]
                taxon_level = fields[6]
                col_taxa[taxon_id] = {'parent_id': parent_id, 'name': taxon_name, 'level': taxon_level}

    logging.info(f'Parsing CoL to retrieve all {base_taxon} species')
    n_species_total_col = 0
    col_orders = set()
    with open(col_taxa_path) as taxon_file:
        taxon_file.readline()
        for line in taxon_file:
            fields = line.split('\t')
            if fields[6] == 'species':
                taxon_id = fields[0]
                parent_id = fields[1]
                taxon_name = fields[7].split(' ')[0]
                if is_base_taxon(parent_id, col_taxa, base_taxon):
                    n_species_total_col += 1
                    col_orders.add(find_order(parent_id, col_taxa))
    output_file.write(f'Total number of Arthropod species in CoL: {n_species_total_col}\n')
    output_file.write(f'Total number of Arthropod orders in CoL: {len(col_orders)}\n')

    # Insecta vs. other arthropods
    logging.info(f'Computing numbers for insecta and other classes')
    output_file.write('\n## Class-based numbers\n')
    class_assemblies_counts = data['Class'].value_counts(dropna=False)
    n_assemblies_insecta = class_assemblies_counts['Insecta']
    output_file.write(f'Number of assemblies for Insecta: {n_assemblies_insecta}\n')
    n_assemblies_other_classes = sum(class_assemblies_counts) - n_assemblies_insecta
    output_file.write(f'Number of assemblies for other classes (non-Insecta): {n_assemblies_other_classes}\n')
    class_species_counts = species_data['Class'].value_counts(dropna=False)
    n_species_insecta = class_species_counts['Insecta']
    output_file.write(f'Number of species for Insecta: {n_species_insecta}\n')
    n_species_other_classes = sum(class_species_counts) - n_species_insecta
    output_file.write(f'Number of species for other classes (non-Insecta): {n_species_other_classes}\n')

    # Most represented orders
    logging.info(f'Computing numbers for most represented orders')
    output_file.write('\n## Most represented orders\n')
    n_assemblies_diptera = order_assemblies_counts['Diptera']
    output_file.write(f'Number of assemblies for Diptera: {n_assemblies_diptera}\n')
    n_assemblies_lepidoptera = order_assemblies_counts['Lepidoptera']
    output_file.write(f'Number of assemblies for Lepidoptera: {n_assemblies_lepidoptera}\n')
    n_assemblies_hymenoptera = order_assemblies_counts['Hymenoptera']
    output_file.write(f'Number of assemblies for Hymenoptera: {n_assemblies_hymenoptera}\n')
    n_species_diptera = order_species_counts['Diptera']
    output_file.write(f'Number of species for Diptera: {n_species_diptera}\n')
    n_species_lepidoptera = order_species_counts['Lepidoptera']
    output_file.write(f'Number of species for Lepidoptera: {n_species_lepidoptera}\n')
    n_species_hymenoptera = order_species_counts['Hymenoptera']
    output_file.write(f'Number of species for Hymenoptera: {n_species_hymenoptera}\n')
    most_represented_orders_assemblies_percent = (n_assemblies_diptera + n_assemblies_lepidoptera + n_assemblies_hymenoptera) / n_assemblies_total * 100
    output_file.write(f'Percent of assemblies included in these three orders: {round(most_represented_orders_assemblies_percent, 1)} %\n')
    most_represented_orders_species_percent = (n_species_diptera + n_species_lepidoptera + n_species_hymenoptera) / n_species_total * 100
    output_file.write(f'Percent of species included in these three orders: {round(most_represented_orders_species_percent, 1)} %\n')
    n_species_diptera_col = species_numbers_data.loc[species_numbers_data['Order'] == 'Diptera']['COL'].iloc[0]
    n_species_diptera_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == 'Diptera']['NCBI'].iloc[0]
    output_file.write(f'Total number of species for Diptera: {n_species_diptera_col} (CoL), {n_species_diptera_ncbi} (NCBI)\n')
    n_species_lepidoptera_col = species_numbers_data.loc[species_numbers_data['Order'] == 'Lepidoptera']['COL'].iloc[0]
    n_species_lepidoptera_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == 'Lepidoptera']['NCBI'].iloc[0]
    output_file.write(f'Total number of species for Lepidoptera: {n_species_lepidoptera_col} (CoL), {n_species_lepidoptera_ncbi} (NCBI)\n')
    n_species_hymenoptera_col = species_numbers_data.loc[species_numbers_data['Order'] == 'Hymenoptera']['COL'].iloc[0]
    n_species_hymenoptera_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == 'Hymenoptera']['NCBI'].iloc[0]
    output_file.write(f'Total number of species for Hymenoptera: {n_species_hymenoptera_col} (CoL), {n_species_hymenoptera_ncbi} (NCBI)\n')
    n_species_diptera_col_percent = n_species_diptera_col * 100 / n_species_total_col
    n_species_diptera_ncbi_percent = n_species_diptera_ncbi * 100 / n_species_total_ncbi
    output_file.write(f'Percent number of species for Diptera: {round(n_species_diptera_col_percent, 1)} % (CoL), {round(n_species_diptera_ncbi_percent, 1)} % (NCBI)\n')
    n_species_lepidoptera_col_percent = n_species_lepidoptera_col * 100 / n_species_total_col
    n_species_lepidoptera_ncbi_percent = n_species_lepidoptera_ncbi * 100 / n_species_total_ncbi
    output_file.write(f'Percent number of species for Lepidoptera: {round(n_species_lepidoptera_col_percent, 1)} % (CoL), {round(n_species_lepidoptera_ncbi_percent, 1)} % (NCBI)\n')
    n_species_hymenoptera_col_percent = n_species_hymenoptera_col * 100 / n_species_total_col
    n_species_hymenoptera_ncbi_percent = n_species_hymenoptera_ncbi * 100 / n_species_total_ncbi
    output_file.write(f'Percent number of species for Hymenoptera: {round(n_species_hymenoptera_col_percent, 1)} % (CoL), {round(n_species_hymenoptera_ncbi_percent, 1)} % (NCBI)\n')
    n_species_diptera_col_percent_sequenced = n_species_diptera * 100 / n_species_total_col
    n_species_diptera_ncbi_percent_sequenced = n_species_diptera * 100 / n_species_total_ncbi
    output_file.write(f'Percent sequenced species for Diptera: {round(n_species_diptera_col_percent_sequenced, 3)} % (CoL), {round(n_species_diptera_ncbi_percent_sequenced, 3)} % (NCBI)\n')
    n_species_lepidoptera_col_percent_sequenced = n_species_lepidoptera * 100 / n_species_total_col
    n_species_lepidoptera_ncbi_percent_sequenced = n_species_lepidoptera * 100 / n_species_total_ncbi
    output_file.write(f'Percent sequenced species for Lepidoptera: {round(n_species_lepidoptera_col_percent_sequenced, 3)} % (CoL), {round(n_species_lepidoptera_ncbi_percent_sequenced, 3)} % (NCBI)\n')
    n_species_hymenoptera_col_percent_sequenced = n_species_hymenoptera * 100 / n_species_total_col
    n_species_hymenoptera_ncbi_percent_sequenced = n_species_hymenoptera * 100 / n_species_total_ncbi
    output_file.write(f'Percent sequenced species for Hymenoptera: {round(n_species_hymenoptera_col_percent_sequenced, 3)} % (CoL), {round(n_species_hymenoptera_ncbi_percent_sequenced, 3)} % (NCBI)\n')

    # Coleoptera
    logging.info(f'Computing numbers for Coleoptera')
    output_file.write('\n## Numbers for Coleoptera\n')
    n_species_coleoptera = order_species_counts['Coleoptera']
    output_file.write(f'Number of species with assemblies for Coleoptera: {n_species_coleoptera}\n')
    n_species_coleoptera_col = species_numbers_data.loc[species_numbers_data['Order'] == 'Coleoptera']['COL'].iloc[0]
    n_species_coleoptera_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == 'Coleoptera']['NCBI'].iloc[0]
    output_file.write(f'Total number of species for Coleoptera: {n_species_coleoptera_col} (CoL), {n_species_coleoptera_ncbi} (NCBI)\n')
    n_species_coleoptera_col_percent = n_species_coleoptera_col * 100 / n_species_total_col
    n_species_coleoptera_ncbi_percent = n_species_coleoptera_ncbi * 100 / n_species_total_ncbi
    output_file.write(f'Percent number of species for Coleoptera: {round(n_species_coleoptera_col_percent, 1)} % (CoL), {round(n_species_coleoptera_ncbi_percent, 1)} % (NCBI)\n')
    n_species_coleoptera_col_percent_sequenced = n_species_coleoptera * 100 / n_species_total_col
    n_species_coleoptera_ncbi_percent_sequenced = n_species_coleoptera * 100 / n_species_total_ncbi
    output_file.write(f'Percent sequenced species for Coleoptera: {round(n_species_coleoptera_col_percent_sequenced, 3)} % (CoL), {round(n_species_coleoptera_ncbi_percent_sequenced, 3)} % (NCBI)\n')

    # Retained orders (Fig 1 tree)
    logging.info(f'Computing numbers for Figure 1')
    output_file.write('\n## Numbers for Figure 1\n')
    n_orders_retained = species_numbers_data.shape[0]
    output_file.write(f'Number of orders retained for Figure 1: {n_orders_retained}\n')

    # Species with lots of assemblies
    logging.info(f'Computing numbers for species with more than one assembly')
    output_file.write('\n## Species with many assemblies\n')
    species_assemblies_counts_lepidoptera = data.loc[data['Order'] == 'Lepidoptera']['Species'].value_counts(dropna=False)
    most_represented_species_lepidoptera = species_assemblies_counts_lepidoptera[species_assemblies_counts_lepidoptera >= min_assemblies_per_species]
    output_file.write(f'Species with most assemblies for Lepidoptera (>= {min_assemblies_per_species}):\n')
    for species, assemblies in most_represented_species_lepidoptera.iteritems():
        output_file.write(f'    - {species}: {assemblies}\n')
    species_assemblies_counts_diptera = data.loc[data['Order'] == 'Diptera']['Species'].value_counts(dropna=False)
    most_represented_species_diptera = species_assemblies_counts_diptera[species_assemblies_counts_diptera >= min_assemblies_per_species]
    output_file.write(f'Species with most assemblies for Diptera (>= {min_assemblies_per_species}):\n')
    for species, assemblies in most_represented_species_diptera.iteritems():
        output_file.write(f'    - {species}: {assemblies}\n')

    # Assembly level
    logging.info(f'Computing numbers for assembly level')
    output_file.write('\n## Assembly level numbers\n')
    assembly_level_counts = data['Assembly Level'].value_counts(dropna=False)
    output_file.write(f'Assembly level counts:\n')
    for level, count in assembly_level_counts.iteritems():
        output_file.write(f'    - {level}: {count} ({round(count / n_assemblies_total * 100, 1)} %)\n')
    species_counts_all = data['Species'].value_counts(dropna=False)
    species_with_multiple_assemblies = set((s for s, v in species_counts_all.iteritems() if v > 1))
    output_file.write(f'Species with more than 1 assemblies: {len(species_with_multiple_assemblies)}\n')
    species_with_multiple_assemblies_chromosome_level = set()
    for index, row in data.iterrows():
        if row['Species'] in species_with_multiple_assemblies and row['Assembly Level'] == 'Chromosome':
            species_with_multiple_assemblies_chromosome_level.add(row['Species'])
    output_file.write(f'Species with more than 1 assemblies and at least 1 chromosome level: {len(species_with_multiple_assemblies_chromosome_level)}\n')

    # Assembly N50 stats
    logging.info(f'Computing numbers for assembly N50')
    output_file.write('\n## Assembly N50 numbers\n')
    n50_data = data[['Order', 'Scaffold N50']]
    median_n50_per_order = n50_data.groupby('Order').median()
    lowest_median_order, lowest_median_value = median_n50_per_order['Scaffold N50'].idxmin(), median_n50_per_order['Scaffold N50'].min()
    lowest_median_assemblies = order_assemblies_counts[lowest_median_order]
    lowest_median_species = order_species_counts[lowest_median_order]
    lowest_median_species_col = species_numbers_data.loc[species_numbers_data['Order'] == lowest_median_order]['COL'].iloc[0]
    lowest_median_species_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == lowest_median_order]['NCBI'].iloc[0]
    output_file.write(f'Lowest median N50: {lowest_median_value} - {lowest_median_order} - {lowest_median_assemblies} assemblies, {lowest_median_species} species, {lowest_median_species_col} species (CoL), {lowest_median_species_ncbi} species (NCBI)\n')
    highest_median_order, highest_median_value = median_n50_per_order['Scaffold N50'].idxmax(), median_n50_per_order['Scaffold N50'].max()
    highest_median_assemblies = order_assemblies_counts[highest_median_order]
    highest_median_species = order_species_counts[highest_median_order]
    highest_median_species_col = species_numbers_data.loc[species_numbers_data['Order'] == highest_median_order]['COL'].iloc[0]
    highest_median_species_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == highest_median_order]['NCBI'].iloc[0]
    output_file.write(f'Highest median N50: {highest_median_value} - {highest_median_order} - {highest_median_assemblies} assemblies, {highest_median_species} species, {highest_median_species_col} species (CoL), {highest_median_species_ncbi} species (NCBI)\n')
    orders_more_than_5_assemblies = set(order_assemblies_counts[order_assemblies_counts >= min_assemblies_per_order].index.values)
    orders_more_than_5_assemblies.remove('Lepidoptera')
    median_n50_per_order_5_assemblies = median_n50_per_order.loc[orders_more_than_5_assemblies]
    lowest_median_order_5_assemblies, lowest_median_value_5_assemblies = median_n50_per_order_5_assemblies['Scaffold N50'].idxmin(), median_n50_per_order_5_assemblies['Scaffold N50'].min()
    lowest_median_assemblies_5_assemblies = order_assemblies_counts[lowest_median_order_5_assemblies]
    lowest_median_species_5_assemblies = order_species_counts[lowest_median_order_5_assemblies]
    lowest_median_species_5_assemblies_col = species_numbers_data.loc[species_numbers_data['Order'] == lowest_median_order_5_assemblies]['COL'].iloc[0]
    lowest_median_species_5_assemblies_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == lowest_median_order_5_assemblies]['NCBI'].iloc[0]
    output_file.write(f'Lowest median N50 (>5 assemblies): {lowest_median_value_5_assemblies} - {lowest_median_order_5_assemblies} - {lowest_median_assemblies_5_assemblies} assemblies, {lowest_median_species_5_assemblies} species, {lowest_median_species_5_assemblies_col} species (CoL), {lowest_median_species_5_assemblies_ncbi} species (NCBI)\n')
    highest_median_order_5_assemblies, highest_median_value_5_assemblies = median_n50_per_order_5_assemblies['Scaffold N50'].idxmax(), median_n50_per_order_5_assemblies['Scaffold N50'].max()
    highest_median_assemblies_5_assemblies = order_assemblies_counts[highest_median_order_5_assemblies]
    highest_median_species_5_assemblies = order_species_counts[highest_median_order_5_assemblies]
    highest_median_species_5_assemblies_col = species_numbers_data.loc[species_numbers_data['Order'] == highest_median_order_5_assemblies]['COL'].iloc[0]
    highest_median_species_5_assemblies_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == highest_median_order_5_assemblies]['NCBI'].iloc[0]
    output_file.write(f'Highest median N50 (>5 assemblies): {highest_median_value_5_assemblies} - {highest_median_order_5_assemblies} - {highest_median_assemblies_5_assemblies} assemblies, {highest_median_species_5_assemblies} species, {highest_median_species_5_assemblies_col} species (CoL), {highest_median_species_5_assemblies_ncbi} species (NCBI)\n')

    # BUSCO stats
    logging.info(f'Computing numbers for BUSCO results')
    output_file.write('\n## BUSCO numbers\n')
    busco_data = data[['Order', 'Arthropoda Complete BUSCO']]
    median_busco_per_order = busco_data.groupby('Order').median()
    orders_more_than_5_assemblies = set(order_assemblies_counts[order_assemblies_counts >= min_assemblies_per_order].index.values)
    orders_more_than_5_assemblies.remove('Lepidoptera')
    median_busco_per_order_5_assemblies = median_busco_per_order.loc[orders_more_than_5_assemblies]
    lowest_median_order_5_assemblies, lowest_median_value_5_assemblies = median_busco_per_order_5_assemblies['Arthropoda Complete BUSCO'].idxmin(), median_busco_per_order_5_assemblies['Arthropoda Complete BUSCO'].min()
    lowest_median_assemblies_5_assemblies = order_assemblies_counts[lowest_median_order_5_assemblies]
    lowest_median_species_5_assemblies = order_species_counts[lowest_median_order_5_assemblies]
    lowest_median_species_5_assemblies_col = species_numbers_data.loc[species_numbers_data['Order'] == lowest_median_order_5_assemblies]['COL'].iloc[0]
    lowest_median_species_5_assemblies_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == lowest_median_order_5_assemblies]['NCBI'].iloc[0]
    output_file.write(f'Lowest median busco (>5 assemblies): {lowest_median_value_5_assemblies} - {lowest_median_order_5_assemblies} - {lowest_median_assemblies_5_assemblies} assemblies, {lowest_median_species_5_assemblies} species, {lowest_median_species_5_assemblies_col} species (CoL), {lowest_median_species_5_assemblies_ncbi} species (NCBI)\n')
    highest_median_order_5_assemblies, highest_median_value_5_assemblies = median_busco_per_order_5_assemblies['Arthropoda Complete BUSCO'].idxmax(), median_busco_per_order_5_assemblies['Arthropoda Complete BUSCO'].max()
    highest_median_assemblies_5_assemblies = order_assemblies_counts[highest_median_order_5_assemblies]
    highest_median_species_5_assemblies = order_species_counts[highest_median_order_5_assemblies]
    highest_median_species_5_assemblies_col = species_numbers_data.loc[species_numbers_data['Order'] == highest_median_order_5_assemblies]['COL'].iloc[0]
    highest_median_species_5_assemblies_ncbi = species_numbers_data.loc[species_numbers_data['Order'] == highest_median_order_5_assemblies]['NCBI'].iloc[0]
    output_file.write(f'Highest median busco (>5 assemblies): {highest_median_value_5_assemblies} - {highest_median_order_5_assemblies} - {highest_median_assemblies_5_assemblies} assemblies, {highest_median_species_5_assemblies} species, {highest_median_species_5_assemblies_col} species (CoL), {highest_median_species_5_assemblies_ncbi} species (NCBI)\n')

    # EBP reference stats
    logging.info(f'Computing number of assemblies matching EBP criteria (90% BUSCO / annotated as chromosome-level)')
    output_file.write('\n## EBP numbers\n')
    ebp_data = data[['Arthropoda Single BUSCO', 'Assembly Level']]
    ebp_quality = ebp_data[(ebp_data['Arthropoda Single BUSCO'] > 90) & (ebp_data['Assembly Level'] == 'Chromosome')]
    output_file.write(f'Number of assemblies matching EBP criteria: {ebp_quality.shape[0]}')
    # END
    output_file.close()
