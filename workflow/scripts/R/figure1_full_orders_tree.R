# Get log file path from snakemake and redirect output to log
log_file_path <- snakemake@log[[1]]
log_file <- file(log_file_path)
sink(log_file, append=TRUE)
sink(log_file, append=TRUE, type="message")

# Get other info from snakemake
orders_data <- snakemake@input$orders_data
species_file_path <- snakemake@input$species_numbers
tree_file_path <- snakemake@input$orders_tree
time_data_file_path <- snakemake@input$time_data
output_file_path <- snakemake@output[[1]]

# Prevent R from automatically creating the file "Rplots.pdf"
pdf(NULL)

library(ggplot2)
library(cowplot)
library(ggtree)
library(RColorBrewer)
library(readr)
library(ape)

# Load summary file, tree, and orders palette
load(orders_data)

# Load species count
species_count <- read.table(species_file_path, header = T)

# Create a dataframe of number of assemblies for each order
assemblies <- table(data$Order)
assemblies <- data.frame("Order" = names(assemblies), "Assemblies" = as.vector(assemblies))
assemblies$Order <- paste0(toupper(substr(assemblies$Order, 1, 1)), substr(assemblies$Order, 2, 100))  # Format orders

# Merge assembly number and species number data
species_abundance <- merge.data.frame(species_count, assemblies, all = TRUE)
species_abundance$Assemblies[is.na(species_abundance$Assemblies)] <- 0

# Generate a data frame to color background of tip labels
color_data <- data.frame("Taxon"=species_abundance$Order, "Order"=species_abundance$Order)
missing_orders <- color_data$Order[!(color_data$Order %in% names(palette))]
palette <- c(palette, setNames(rep(NA, length(missing_orders)), missing_orders))
alpha_palette <- palette
alpha_palette[which(!is.na(alpha_palette))] <- 1
alpha_palette[which(is.na(alpha_palette))] <- 0
alpha_palette <- setNames(as.numeric(alpha_palette), names(alpha_palette))

# Read full orders tree
tree <- read.tree(file = tree_file_path)

# Plot the tree using ggtree
tree_plot <- ggtree(tree, layout = "roundrect", branch.length = "none") +
    theme(plot.margin = margin(0, 0, 0, 0),
          legend.position = "none") +
    xlim_tree(12) +
    scale_y_continuous(expand = c(0.0025,0))

# Add order colors to the tree
tree_plot <- tree_plot %<+% color_data +
    geom_tiplab(aes(fill = Order, alpha = Order), color = "black", hjust = 0, align = TRUE, offset = 0, label.size = 0, geom = "label") +
    scale_fill_manual(values = palette) +
    scale_alpha_manual(values = alpha_palette)

# Generate inset plot of assemblies over time
time_data <- read_delim(time_data_file_path, '\t', escape_double = FALSE, trim_ws = TRUE)

# Generate a nice time scale for the inset
scale_x_breaks <- seq(9, max(time_data$Months), 24)
scale_x_labels <- seq(2005, 2005 + (length(scale_x_breaks) - 1) * 2, 2)

# Inset color scale
inset_colors <- c("Assemblies"="royalblue3", "Orders"="firebrick3", "Species"="springgreen4")

inset <- ggplot(time_data) +
    geom_ribbon(aes(x = Months, ymin = 0, ymax = Assemblies, fill = "Assemblies", color = "Assemblies"), alpha = 0.6) +
    geom_ribbon(aes(x = Months, ymin = 0, ymax = Species, fill = "Species", color = "Species"), alpha = 0.6) +
    geom_ribbon(aes(x = Months, ymin = 0, ymax = Order, fill = "Orders", color = "Orders"), alpha = 0.6) +
    theme_cowplot() +
    theme(axis.title.y = element_blank(),
          legend.position = c(0.15, 0.5),
          legend.key.size = unit(0.05, 'npc'),
          legend.text = element_text(size = 18)) +
    scale_x_continuous(breaks = scale_x_breaks, labels = scale_x_labels, expand=c(0, 0.1)) +
    scale_y_continuous(position = "right", expand = c(0, 0.1)) +
    scale_color_manual(values = inset_colors, guide = "none") +
    scale_fill_manual(name = "", values = inset_colors, aes(alpha = 0.6)) +
    coord_flip() +
    guides(fill = guide_legend(override.aes = list(alpha = 0.35)))

tree_plot <- tree_plot + annotation_custom(ggplotGrob(inset), xmin = -0.5, xmax = 8, 
                                           ymin = 60, ymax = 115)

# Extract y coordinate for tips from the tree to use for assembly and species column
columns_data <- ggplot_build(tree_plot)$data[[4]][, c("y", "label")]
names(columns_data) <- c("y", "Order")

# Merge y coordinate data frame with species and assembly numbers data frame
columns_data <- merge.data.frame(columns_data, species_abundance, all = TRUE)
columns_data$Order <- factor(columns_data$Order, levels = columns_data$Order[order(columns_data$y)])
columns_data$Fontface <- c("FALSE"="plain", "TRUE"="bold")[as.character(columns_data$Assemblies > 0)]

# Generate the base plot to use for a column
blank_column <- ggplot(columns_data, aes(y = Order)) +
    theme(plot.title = element_text(hjust = 0.5, size = 12, face = "bold"),
          axis.line = element_blank(),
          axis.text = element_blank(),
          axis.ticks = element_blank(),
          axis.title.x = element_blank(),
          axis.title.y = element_blank(),
          legend.position = "none",
          panel.background = element_blank(),
          panel.border = element_blank(),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          plot.margin = margin(0, 0, 0, 0)) +
    scale_y_discrete(expand = c(0.0025,0))

# Generate plot for species column
species_column_ncbi <- blank_column +
    geom_label(aes(x = 1, label = format(NCBI, big.mark = ","), fontface = Fontface, fill = Order, alpha = Order),
               label.size = 0) +
    scale_fill_manual(values = palette) +
    scale_alpha_manual(values = alpha_palette) +
    ggtitle("Sp. (NCBI)")

# Generate plot for species column
species_column_col <- blank_column +
    geom_label(aes(x = 1, label = format(COL, big.mark = ","), fontface = Fontface, fill = Order, alpha = Order),
               label.size = 0) +
    scale_fill_manual(values = palette) +
    scale_alpha_manual(values = alpha_palette) +
    ggtitle("Sp. (CoL)")

# Generate plot for assemblies column
assemblies_column <- blank_column +
    geom_label(aes(x = 1, label = format(Assemblies, big.mark = ","), fontface = Fontface, fill = Order, alpha = Order),
               label.size = 0) +
    scale_fill_manual(values = palette) +
    scale_alpha_manual(values = alpha_palette) +
    ggtitle("Assemblies")

# Combine tree with the two columns
full_plot <- plot_grid(tree_plot, species_column_ncbi, species_column_col, assemblies_column, ncol = 4, align = "h", axis = "h", rel_widths = c(1, 0.1, 0.1, 0.1))

# Save resulting plot
ggsave(output_file_path, full_plot, width = 14, height = 24, bg = "white")
