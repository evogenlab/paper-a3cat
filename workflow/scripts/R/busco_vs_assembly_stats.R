# Get log file path from snakemake and redirect output to log
log_file_path <- snakemake@log[[1]]
log_file <- file(log_file_path)
sink(log_file, append=TRUE)
sink(log_file, append=TRUE, type="message")

# Get other info from snakemake
orders_data <- snakemake@input$orders_data
output_file_path <- snakemake@output[[1]]
lineage <- snakemake@params$lineage[[1]]
metric <- snakemake@params$metric[[1]]
min_size <- snakemake@params$min_assembly_size[1]

# Prevent R from automatically creating the file "Rplots.pdf"
pdf(NULL)

library(cowplot)
library(ggplot2)
library(readr)

# Load summary file, tree, and orders palette
load(orders_data)

# Breaks for log scale
x <- seq(1, 20, 1)
min_size_omag <- floor(log(min_size, 10))
min_size <- floor(min_size / 10^min_size_omag) * 10^min_size_omag

# Check if a busco threshold was specified in params, otherwise sets to NA (for reusability between main and supp figures)
if ('busco_threshold' %in% names(snakemake@params)) busco_threshold_y = snakemake@params$busco_threshold - 1 else busco_threshold_y = NA


# BUSCO score vs assembly N50
busco_vs_n50 <- ggplot(data, aes_string(x = "`Scaffold N50`", y = paste0("`", lineage, " ", metric, " BUSCO`"), fill = "Order_plot", color = "Order_plot")) +
    theme_cowplot()

if (!is.na(busco_threshold_y)) {
    busco_vs_n50 <- busco_vs_n50 +
        annotate("rect", xmin = 0.9*min(data$`Scaffold N50`), xmax = 1.5*max(data$`Scaffold N50`), ymin = busco_threshold_y, ymax = 101, alpha = 0.2, color = "white", fill = "grey80") +
        annotate("segment", x = 0.9*min(data$`Scaffold N50`), xend = 1.5*max(data$`Scaffold N50`), y = busco_threshold_y, yend = busco_threshold_y, color = "grey50", linetype = 3)
}

busco_vs_n50 <- busco_vs_n50 +
    geom_vline(xintercept = 1000, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 10000, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 50000, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 500000, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 5000000, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 25000000, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 250000000, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_point(size = 1.75, pch = 21, alpha = 1) +
    theme(legend.position = "none",
          axis.title.x = element_text(face = "bold"),
          axis.title.y = element_text(face = "bold"),
          axis.line.x.top = element_blank(),
          plot.margin = margin(10, 15, 10, 15)) +
    scale_x_continuous(name = expression(bold(paste("Assembly N50 (bp, log"[bold("10")], ")", sep = ""))),
                       expand = c(0.015, 0),
                       trans = "log10",
                       breaks = 10^x,
                       labels = parse(text = paste("10^", x, sep = "")),
                       sec.axis = sec_axis(trans = ~., breaks = c(1000, 10000, 50000, 500000, 5000000, 25000000, 250000000), labels = c("1Kb", "10Kb", "50Kb", "500Kb", "5Mb", "25Mb", "250Mb"))) +
    scale_y_continuous(name = paste0(metric, " BUSCOs (%)"), expand = c(0.015, 0)) +
    scale_fill_manual(guide = 'none', values = palette) +
    scale_color_manual(values = palette)


# BUSCO score vs assembly size
busco_vs_size <- ggplot(data, aes_string(x = "`Total length`", y = paste0("`", lineage, " ", metric, " BUSCO`"), fill = "Order_plot", color = "Order_plot")) +
    theme_cowplot()

if (!is.na(busco_threshold_y)) {
    busco_vs_size <- busco_vs_size +
        annotate("rect", xmin=min_size, xmax=1.1*max(data$`Total length`), ymin=busco_threshold_y, ymax=101, alpha=0.2, color="white", fill="grey80") +
        annotate("segment", x=min_size, xend=1.1*max(data$`Total length`), y=busco_threshold_y, yend=busco_threshold_y, color="grey50", linetype=3)
}

busco_vs_size <- busco_vs_size +
    geom_vline(xintercept = 2.5*10^6, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 10*10^6, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 50*10^6, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 250*10^6, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 10^9, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_vline(xintercept = 5*10^9, linetype = 2, color = "grey50", size = 0.5, alpha = 0.25) +
    geom_point(size = 1.75, pch = 21, alpha = 1) +
    theme(legend.position = "none",
          legend.title = element_blank(),
          axis.title.x = element_text(face = "bold"),
          axis.title.y = element_text(face = "bold"),
          axis.line.x.top = element_blank(),
          plot.margin = margin(10, 15, 10, 15)) +
    scale_x_continuous(name = expression(bold(paste("Assembly size (bp, log"[bold("10")], ")", sep = ""))),
                       limits = c(min_size, 1.1*max(data$`Total length`)),
                       expand = c(0.01, 0),
                       trans = "log10",
                       breaks = 10^x,
                       labels = parse(text = paste("10^", x, sep = "")),
                       sec.axis = sec_axis(trans = ~., breaks = c(2.5*10^6, 10*10^6, 5*10^7, 25*10^7, 10^9, 5*10^9), labels = c("2.5Mb", "10Mb", "50Mb", "250Mb", "1Gb", "5Gb"))) +
    scale_y_continuous(name = paste0(metric, " BUSCOs (%)"), expand = c(0.015, 0)) +
    scale_fill_manual(name = "Order", guide = guide_legend(nrow = 2, override.aes = list(size = 4)), values = palette) +
    scale_color_manual(name = "Order", values = palette)

# Extract legend from complete_vs_size panel
order_legend <- get_legend(busco_vs_size + theme(legend.position = "bottom",
                                           legend.text = element_text(size = 11.5),
                                           legend.box.margin = margin(l = 5)) +
                               guides(fill = guide_legend(nrow = 2, byrow = TRUE, override.aes = list(size = 4))))

# Combine N50 and assembly size plots
panels <- plot_grid(busco_vs_n50, busco_vs_size, ncol = 2, align = "hv", axis = "lbt", labels = c("A", "B"))

# Combine panels with legend
combined <- ggdraw() +
    draw_plot(panels, x = 0, y = 0.1, 1, 0.9) +
    draw_plot(order_legend, x = 0, y = 0, width = 1, height = 0.1, scale = 1)

# Save final plot
ggsave(output_file_path, combined, width=16, height=8, bg="white")

# Close the pdf device that we sent to NULL
dev.off()
