import collections
import logging
from ete3 import NCBITaxa
from utils import setup_logging, is_base_taxon, find_order

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    taxon_file_path = snakemake.input.col_taxa_path[0]
    name_matching_file_path = snakemake.input.name_matching_file_path
    ete3_db_path = snakemake.input.ete3_db_path[0]
    tree_file_path = snakemake.output.tree
    species_numbers_file_path = snakemake.output.species_numbers
    base_taxon = snakemake.params.base_taxon

    # Getting a tree of orders from NCBI
    logging.info(f'Getting descendants for "{base_taxon}" from NCBI')
    ncbi = NCBITaxa(dbfile=ete3_db_path)
    descendants = ncbi.get_descendant_taxa(base_taxon, intermediate_nodes=True, rank_limit='order', collapse_subspecies=True)
    ranks = ncbi.get_rank(descendants)
    logging.info(f'Filtering descendants to only retain orders for "{base_taxon}"')
    orders = tuple(descendant for descendant in descendants if ranks[descendant] == 'order')
    orders_taxid = ncbi.get_taxid_translator(orders)
    order_names = {v: k for k, v in orders_taxid.items()}

    logging.info(f'Parsing CoL to retrieve all ranks above species')
    col_taxa = {}
    with open(taxon_file_path) as taxon_file:
        taxon_file.readline()
        for line in taxon_file:
            fields = line.split('\t')
            if fields[6] != 'species':
                taxon_id = fields[0]
                parent_id = fields[1]
                taxon_name = fields[7].split(' ')[0]
                taxon_level = fields[6]
                col_taxa[taxon_id] = {'parent_id': parent_id, 'name': taxon_name, 'level': taxon_level}

    logging.info(f'Parsing CoL to retrieve all {base_taxon} species')
    species_counts = collections.defaultdict(lambda: {'ncbi': 0, 'col': 0})
    with open(taxon_file_path) as taxon_file:
        taxon_file.readline()
        for line in taxon_file:
            fields = line.split('\t')
            if fields[6] == 'species':
                taxon_id = fields[0]
                parent_id = fields[1]
                taxon_name = fields[7].split(' ')[0]
                if is_base_taxon(parent_id, col_taxa, base_taxon):
                    order = find_order(parent_id, col_taxa)
                    species_counts[order]['col'] += 1

    logging.info(f'Applying manual corrections for correspondence between CoL and NCBI Taxonomy names')
    with open(name_matching_file_path) as name_matching_file:
        name_matching = {}
        for line in name_matching_file:
            ncbi_name, col_name = line.rstrip('\n').split('\t')
            if col_name != '':
                name_matching[ncbi_name] = col_name
            else:
                del order_names[ncbi_name]

    # Saving number of species to stats file
    logging.info(f'Finding number of species in each order from NCBI Taxonomy')
    for order_name, taxid in order_names.items():
        descendants = ncbi.get_descendant_taxa(taxid)
        ranks = ncbi.get_rank(descendants)
        descendants_names = ncbi.get_taxid_translator(descendants)
        species = tuple(descendant for descendant in descendants if
                        ranks[descendant] == 'species')
        col_order = order_name
        if order_name in name_matching:
            col_order = name_matching[order_name]
        species_counts[col_order]['ncbi'] = len(species)

    logging.info(f'Writing species numbers to output file {species_numbers_file_path}')
    with open(species_numbers_file_path, 'w') as output_file:
        output_file.write('Order\tNCBI\tCOL\n')
        for order in order_names:
            col_order = order
            if order in name_matching:
                col_order = name_matching[order]
            output_file.write(f'{order}\t{species_counts[col_order]["ncbi"]}\t{species_counts[col_order]["col"]}\n')

    logging.info(f'Generating updated order tree for {base_taxon}')
    tree = ncbi.get_topology(order_names.values())
    for node in tree.traverse():
        node.name = node.sci_name
    logging.info(f'Saving tree to <{tree_file_path}>')
    tree_file = open(tree_file_path, 'w')
    tree_file.write(tree.write(format=3))
    tree_file.close()
