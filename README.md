# A3Cat paper workflow

This workflow implements all analyses presented in the manuscript [Assessing species coverage and assembly quality of rapidly accumulating sequenced genomes](https://www.biorxiv.org/content/10.1101/2021.10.15.464561v1). The workflow generates all figures except Supplementary figure 1 (flowchart describing the main workflow) and computes all numbers reported in the manuscript. It is implemented using the Snakemake workflow management engine and dependencies are handled with Conda.

## Setup

Clone the repository from gitlab:

```bash
# Using https (default way, no setup required)
https://gitlab.com/evogenlab/paper-a3cat.git
```

```bash
# Using ssh (recommended way, requires prior setup)
git@gitlab.com:evogenlab/paper-a3cat.git
```

If you do not have Snakemake and Conda already setup, please follow [the instructions below](#setting-up-conda-and-snakemake). The workflow was tested with **Snakemake v. 6.3.0**.

## Executing the workflow

Simply execute Snakemake from the main workflow directory (*i.e.* the `paper-a3cat` directory created after cloning the repository):

```bash
# You can use more than one core to parallelize jobs by changing the value of --cores
snakemake --cores 1 --use-conda
```

## Output

After running the workflow, a `results` directory will contain all figures named after their order in the manuscript. All numbers reported in the manuscript are saved in the file `results/data/paper_numbers.tsv`.

## Setting up Conda and Snakemake

### Install Conda

Full instructions available [here](https://conda.io/projects/conda/en/latest/user-guide/install/linux.html).

**Download the installer script and run it:**

```bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Follow instructions from the prompt. 

**Restart your shell:**

```bash
source ~/.bashrc
```

**Note:** If you're not using bash as your shell, source the appropriate file (*e.g.* `~/.zshrc` if you're using zsh)

**Initialize conda for your shell:**

```bash
# Replace bash with whatever shell you are using
conda init bash
```

**Update conda:**

The Conda version from the official installer is not always the latest update. To make sure Conda is up-to-date, run:

```bash
conda update conda
```

### Install Snakemake

The recommended way to install and run Snakemake is to create a conda environment specifically for it:

```bash
# Create a new empty environment called "snakemake"
conda create --name snakemake
# Activate the environment "snakemake"
conda activate snakemake
# Install snakemake from the Bioconda channel (conda-forge contains dependencies)
conda install -c conda-forge -c bioconda snakemake
```

You can now activate the environment snakemake and run snakemake from it. It is advised to keep the environment as clean as possible, *i.e.* only install software related to running snakemake.

You may need to install [mamba]() (an efficient implementation of conda) because Snakemake uses mamba by default since version 6.1.0. To do so, run 

```bash
conda install -n base -c conda-forge mamba
```
